import React from "react";
import "./Frame2.css";

function Frame2(props) {
  const {
    title,
    giPhnHi,
    text1,
    vector1,
    text7,
    vector2,
    vector3,
    vector4,
    vector5,
    vector6,
    vector7,
    vector8,
    vector9,
    vector10,
    vector11,
    vector12,
    vector13,
    vector14,
    vector15,
    vector16,
    vector17,
    vector18,
    vector19,
    vector20,
    vector21,
    vector22,
    vector23,
    vector24,
    vector25,
    vector26,
    vector27,
    vector28,
    vector29,
    vector30,
    vector31,
    vector32,
    vector33,
    vector34,
    vector35,
    vector36,
    vector37,
    vector38,
    vector39,
    vector40,
    vector41,
    vector42,
    vector43,
    vector44,
    vector45,
    vector46,
    vector47,
    vector48,
    vector49,
    vector50,
    vector51,
    vector52,
    vector53,
    vector54,
    vector55,
    vector56,
    vector57,
    vector58,
    vector59,
    vector60,
    vector61,
    vector62,
    vector63,
    vector64,
    vector65,
    vector66,
    vector67,
    vector68,
    vector69,
    vector70,
    vector71,
    vector72,
    vector73,
    vector74,
    vector75,
    vector76,
    vector77,
    vector78,
    vector79,
    vector80,
    vector81,
    vector82,
    vector83,
    vector84,
    vector85,
    vector86,
    vector87,
    vector88,
    vector89,
    vector90,
    vector91,
    vector92,
    vector93,
    vector94,
    vector95,
    vector96,
    vector97,
    vector98,
    vector99,
    vector100,
    vector101,
    vector102,
    vector103,
    vector104,
    vector105,
    vector106,
    vector107,
    vector108,
    vector109,
    vector110,
    vector111,
    vector112,
    vector113,
    vector114,
    vector115,
    vector116,
    vector117,
    vector118,
    vector119,
    vector120,
    vector121,
    vector122,
    vector123,
    vector124,
    vector125,
    text2,
    spanText,
    spanText2,
    vector126,
    vector127,
    text3,
    vector128,
    text5,
    text4,
    spanText3,
    spanText4,
    spanText5,
    spanText6,
    text8,
    text9,
    vector129,
    giPhnNh,
  } = props;

  return (
    <div class="container-center-horizontal">
      <div className="frame-2 screen">
        <div className="flex-col">
          <div className="overlap-group4">
            <h1 className="title roboto-bold-blue-whale-24px">{title}</h1>
            <img
              className="vector-86"
              src="https://anima-uploads.s3.amazonaws.com/projects/60812c070afd67277761228b/releases/60812c60d5fcd14c2857017a/img/vector@2x.svg"
            />
          </div>
          <div className="overlap-group8">
            <div className="gi-phn-hi">{giPhnHi}</div>
            <div className="overlap-group3">
              <div className="text-1 valign-text-middle roboto-normal-black-18px">{text1}</div>
              <img className="vector-1" src={vector1} />
            </div>
            <div className="text-7 valign-text-middle roboto-normal-green-blue-18px">{text7}</div>
            <div className="asset-4-1">
              <div className="overlap-group5">
                <img className="vector-109" src={vector2} />
                <img className="vector-89" src={vector3} />
                <img className="vector-44" src={vector4} />
                <img className="vector-113" src={vector5} />
                <img className="vector-20" src={vector6} />
                <img className="vector-51" src={vector7} />
                <img className="vector-91" src={vector8} />
                <img className="vector-94" src={vector9} />
                <img className="vector-107" src={vector10} />
                <img className="vector-96" src={vector11} />
                <img className="vector-8" src={vector12} />
                <img className="vector-43" src={vector13} />
                <img className="vector-93" src={vector14} />
                <img className="vector-69" src={vector15} />
                <img className="vector-68" src={vector16} />
                <img className="vector-67" src={vector17} />
                <img className="vector-38" src={vector18} />
                <img className="vector-52" src={vector19} />
                <img className="vector-115" src={vector20} />
                <img className="vector-60" src={vector21} />
                <img className="vector-30" src={vector22} />
                <img className="vector-72" src={vector23} />
                <img className="vector-71" src={vector24} />
                <img className="vector-10" src={vector25} />
                <img className="vector-34" src={vector26} />
                <img className="vector-17" src={vector27} />
                <img className="vector-33" src={vector28} />
                <img className="vector" src={vector29} />
                <img className="vector-46" src={vector30} />
                <img className="vector-66" src={vector31} />
                <img className="vector-18" src={vector32} />
                <img className="vector-65" src={vector33} />
                <img className="vector-64" src={vector34} />
                <img className="vector-73" src={vector35} />
                <img className="vector-110" src={vector36} />
                <img className="vector-80" src={vector37} />
                <img className="vector-88" src={vector38} />
                <img className="vector-102" src={vector39} />
                <img className="vector-6" src={vector40} />
                <img className="vector-104" src={vector41} />
                <img className="vector-53" src={vector42} />
                <img className="vector-21" src={vector43} />
                <img className="vector-7" src={vector44} />
                <img className="vector-59" src={vector45} />
                <img className="vector-35" src={vector46} />
                <img className="vector-117" src={vector47} />
                <img className="vector-106" src={vector48} />
                <img className="vector-121" src={vector49} />
                <img className="vector-16" src={vector50} />
                <img className="vector-124" src={vector51} />
                <img className="vector-98" src={vector52} />
                <img className="vector-37" src={vector53} />
                <img className="vector-76" src={vector54} />
                <img className="vector-55" src={vector55} />
                <img className="vector-119" src={vector56} />
                <img className="vector-116" src={vector57} />
                <img className="vector-25" src={vector58} />
                <img className="vector-125" src={vector59} />
                <img className="vector-101" src={vector60} />
                <img className="vector-82" src={vector61} />
                <img className="vector-15" src={vector62} />
                <img className="vector-39" src={vector63} />
                <img className="vector-83" src={vector64} />
                <img className="vector-32" src={vector65} />
                <img className="vector-19" src={vector66} />
                <img className="vector-14" src={vector67} />
                <img className="vector-48" src={vector68} />
                <img className="vector-61" src={vector69} />
                <img className="vector-5" src={vector70} />
                <img className="vector-62" src={vector71} />
                <img className="vector-95" src={vector72} />
                <img className="vector-108" src={vector73} />
                <img className="vector-54" src={vector74} />
                <div className="group-1">
                  <div className="overlap-group-1">
                    <img className="vector-2" src={vector75} />
                    <img className="vector-3" src={vector76} />
                  </div>
                </div>
                <img className="vector-114" src={vector77} />
                <img className="vector-118" src={vector78} />
                <img className="vector-47" src={vector79} />
                <img className="vector-41" src={vector80} />
                <img className="vector-26" src={vector81} />
                <img className="vector-22" src={vector82} />
                <img className="vector-97" src={vector83} />
                <img className="vector-40" src={vector84} />
                <img className="vector-28" src={vector85} />
                <div className="group">
                  <div className="overlap-group-1">
                    <img className="vector-2" src={vector86} />
                    <img className="vector-3" src={vector87} />
                  </div>
                </div>
                <img className="vector-36" src={vector88} />
                <img className="vector-31" src={vector89} />
                <img className="vector-58" src={vector90} />
                <img className="vector-87" src={vector91} />
                <img className="vector-74" src={vector92} />
                <img className="vector-12" src={vector93} />
                <img className="vector-9" src={vector94} />
                <img className="vector-42" src={vector95} />
                <img className="vector-99" src={vector96} />
                <img className="vector-123" src={vector97} />
                <img className="vector-50" src={vector98} />
                <img className="vector-4" src={vector99} />
                <img className="vector-4" src={vector100} />
                <img className="vector-105" src={vector101} />
                <img className="vector-81" src={vector102} />
                <img className="vector-70" src={vector103} />
                <img className="vector-23" src={vector104} />
                <img className="vector-24" src={vector105} />
                <img className="vector-63" src={vector106} />
                <img className="vector-84" src={vector107} />
                <img className="vector-56" src={vector108} />
                <img className="vector-27" src={vector109} />
                <img className="vector-78" src={vector110} />
                <img className="vector-85" src={vector111} />
                <img className="vector-57" src={vector112} />
                <img className="vector-122" src={vector113} />
                <img className="vector-11" src={vector114} />
                <img className="vector-29" src={vector115} />
                <img className="vector-77" src={vector116} />
                <img className="vector-126" src={vector117} />
                <img className="vector-45" src={vector118} />
                <img className="vector-100" src={vector119} />
                <img className="vector-111" src={vector120} />
              </div>
              <img className="vector-103" src={vector121} />
              <img className="vector-92" src={vector122} />
              <img className="vector-49" src={vector123} />
              <img className="vector-120" src={vector124} />
              <img className="vector-75" src={vector125} />
            </div>
          </div>
        </div>
        <div className="flex-col-1">
          <div className="overlap-group9">
            <div className="overlap-group2">
              <div className="text-2 valign-text-middle roboto-normal-black-18px">{text2}</div>
              <div className="ni-dung-phn-nh valign-text-middle roboto-bold-white-18px">
                <span>
                  <span className="span0">{spanText}</span>
                  <span className="span1">{spanText2}</span>
                </span>
              </div>
            </div>
            <img className="vector-79" src={vector126} />
          </div>
          <div className="flex-row">
            <img className="vector-90" src={vector127} />
            <div className="text-3 valign-text-middle roboto-normal-black-18px">{text3}</div>
          </div>
          <div className="flex-row-1">
            <img className="vector-112" src={vector128} />
            <div className="text-5 valign-text-middle roboto-normal-black-18px">{text5}</div>
          </div>
          <div className="overlap-group10">
            <div className="overlap-group1">
              <div className="text-4 valign-text-middle roboto-normal-black-18px">{text4}</div>
              <div className="text-6 valign-text-middle roboto-bold-white-18px">
                <span>
                  <span className="span0">{spanText3}</span>
                  <span className="span1">{spanText4}</span>
                </span>
              </div>
              <div className="thng-tin-lin-h valign-text-middle roboto-bold-white-18px">
                <span>
                  <span className="span0">{spanText5}</span>
                  <span className="span1">{spanText6}</span>
                </span>
              </div>
              <div className="rectangle-5 border-1px-aqua-pearl"></div>
              <div className="text-8">{text8}</div>
              <div className="text-9 roboto-normal-green-blue-18px">{text9}</div>
            </div>
            <img className="vector-13" src={vector129} />
          </div>
          <div className="rectangle-6 border-1px-aqua-pearl"></div>
          <div className="overlap-group">
            <div className="rectangle-5-1"></div>
            <div className="gi-phn-nh valign-text-middle roboto-normal-white-18px">{giPhnNh}</div>
          </div>
        </div>
      </div>
    </div>
  );
}

export default Frame2;
